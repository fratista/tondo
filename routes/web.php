<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Content;

Route::get('/', function () {
    $content = Content::fetchContent();

    return view('configurator')->with('content', $content);
});

Route::group(
    [
    'prefix' => 'configurator'
    ],
    function () {
        Route::post('/calculate', 'ConfiguratorController@calculate');
        Route::post('/send-offer', 'ConfiguratorController@sendOffer');
        Route::get('/preview-offer', 'ConfiguratorController@previewOffer');
    }
);
