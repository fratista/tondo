<nav class="navbar navbar-expand-lg">
  <a class="navbar-brand" href="https://tondo.at">
    <img src="/images/logo.png" alt="Tondo">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="https://www.tondo.at/troege-nach-mass.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Tondo Tröge nach Maß
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Konfigurator</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://www.tondo.at/galerie.php">Galerie</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://www.tondo.at/troege.php">Tröge (nicht konfigurierbar)</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="https://www.tondo.at/beratung-service.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Beratung und Service
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="https://www.tondo.at/zustellung-tondo.php">Zustellung</a>
        </div>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="https://www.tondo.at/hinweise.php">Hinweise</a>
        </div>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="https://www.tondo.at/faq.php">FAQ</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="https://www.tondo.at/baueternit.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Baueternit Zuschnitte
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="https://www.tondo.at/zustellung.php">Baueternit Zustellung</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://www.tondo.at/referenzen.php">Referenzen</a>
      </li>
    </ul>
  </div>
</nav>
