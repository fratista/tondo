<optgroup label="Gelb">
  <option value="d1bc8a">1001 - Beige</option>
  <option value="e1a100">1006 - Maisgelb</option>
</optgroup>
<optgroup label="Rot">
  <option value="a02128">3001 - Signalrot</option>
  <option value="701f29">3004 - Purpurrrot</option>
</optgroup>
<optgroup label="Violett">
  <option value="7e63a1">4005 - Blaulila</option>
  <option value="912d76">4006 - Verkehrspurpur</option>
</optgroup>
<optgroup label="Blau">
  <option value="00427f">5010 - Enzianblau</option>
  <option value="6391b0">5024 - Pastellblau</option>
</optgroup>
<optgroup label="Gr&uuml;n">
  <option value="4d8542">6017 - Maigr&uuml;n</option>
</optgroup>
<optgroup label="Grau">
  <option value="766a5d">7006 - Beigegrau</option>
</optgroup>
<optgroup label="Braun">
  <option value="5b3927">8011 - Nussbraun</option>
</optgroup>
<optgroup label="Wei&szlig; und Schwarz">
  <option value="eee9da">9001 - Cremeweiß</option>
</optgroup>
