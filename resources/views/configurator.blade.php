@extends('app')

@php
    $_SESSION['trough-price'] = 401.01;
    $_SESSION['surface'] = 3.5;

    $delivery = $content[0]->content;
    $material = $content[1]->content;    
@endphp

@section('content')

<div class="technical-wrapper">
    <div class="row">
        <div class="col-small-2">
            <p>{!! $material !!}</p>
        </div class="col-small-2">
        <div class="data-wrapper">
            <ul class="parameter-wrapper">
                <li class="topParam tooltip tooltipstered">
                    <div class="paramDataContainer">
                        <div class="paramValue tn-weight">39</div>
                        <div class="paramUnit">kg</div>
                    </div>
                    <div class="paramText">Gewicht</div>
                </li>
                <li class="topParam tooltip tooltipstered">
                    <div class="paramDataContainer">
                        <div class="paramValue tn-weight-earth">294</div>
                        <div class="paramUnit">kg</div>
                    </div>
                    <div class="paramText">Gewicht mit<br>Erdbefüllung</div>
                </li>
                <li class="topParam tooltip tooltipstered">
                    <div class="paramDataContainer">
                        <div class="paramValue tn-weight-wet">440</div>
                        <div class="paramUnit">kg</div>
                    </div>
                    <div class="paramText">Gewicht<br>Feuchterde</div>
                </li>
                <li class="topParam tooltip tooltipstered">
                    <div class="paramDataContainer">
                        <div class="paramValue tn-volume">212</div>
                        <div class="paramUnit">l</div>
                    </div>
                    <div class="paramText">Volumen</div>
                </li>
                <li class="topParam tooltip tooltipstered">
                    <div class="paramDataContainer">
                        <div class="paramValue tn-surface">3.9</div>
                        <div class="paramUnit">m²</div>
                    </div>
                    <div class="paramText">Oberfläche</div>
                </li>
                <!-- 
                <li class="topParam tooltip tooltipstered">
                    <div class="paramDataContainer">
                        <div class="paramValue tn-bending-wall">2</div>
                        <div class="paramUnit">mm</div>
                    </div>
                    <div class="paramText">Durchbiegung</div>                
                </li>
                -->
            </ul>
        </div>
        <div class="col-small-2">
            <p>{!! $delivery !!}</p>
        </div>
    </div>
</div>

<div class="rendering-wrapper">
    <div id="configurator-background">
        <div class="loader invisible">
            <div class="loader-line"
                style="-webkit-transform: scaleX(1); -moz-transform: scaleX(1); -ms-transform: scaleX(1); -o-transform: scaleX(1);transform: scaleX(1);">
            </div>
        </div>
        <div class="error-wrapper">
            <h5></h5>
        </div>
    </div>
    <div class="container rendering-price-container">
        <div class="row">
            <div class="col-xs-12 col-lg-10" id="rendering">
                <!-- Rendering -->
            </div>
            <div class="d-none d-lg-block col-lg-2">
                <div class="price-box">
                    <div class="price">{{ $_SESSION['trough-price'] }} €</div>
                    <div class="price-vat"><span
                            class="price-value">{{ $_SESSION['trough-price'] * 1.2 }}
                            €</span><span class="vat-text"> inkl. MWSt</span></div>
                    <div class="add-to-cart-buttons">
                        <button type="button" title="Jetzt Anfragen" class="btn btn-tondo send-offer"
                            data-toggle="modal" data-target="#request-modal">
                            <div class="hover-box"
                                style="opacity: 0.152065; transform: translate(-100%, 0%) matrix(1, 0, 0, 1, 0, 0);">
                            </div>
                            <span><span>Jetzt Anfragen</span></span>
                        </button>
                    </div>
                    <div class="price-extras">
                        <span class="delivery-time">mind. 5 Wochen Lieferzeit</span>
                        <span class="tax-details">Lieferung per Spedition (exkl. Lieferkosten)</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="rendering-controls">
    <span class="control zoom-in" title="Hereinzoomen">..</span>
    <span class="control zoom-out" title="Herauszoomen">..</span>
    <span class="control camera-front" title="Nach rechts drehen">..</span>
    <span class="control camera-rear" title="Nach links drehen">..</span>
    <span class="control camera-bottom" title="Nach unten drehen">..</span>
    <span class="control camera-top" title="Nach oben drehen">..</span>
</div>

<div class="configuration-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-4">
                <div class="instructions">Außenmaße</div>
            </div>
            <div class="col-xs-12 col-lg-4">
                <div class="buttons">
                    <div data-toggle="tooltip" data-placement="top" title="Außenmaße"
                        class="pager-button button-measurement active">
                        <div class="bg"></div>
                        <i class="icon-measurement"></i>
                    </div>
                    <div data-toggle="tooltip" data-placement="top" title="Wandoptionen"
                        class="pager-button button-wall">
                        <div class="bg"></div>
                        <i class="icon-wall"></i>
                    </div>
                    <div data-toggle="tooltip" data-placement="top" title="Fußoptionen"
                        class="pager-button button-foot">
                        <div class="bg"></div>
                        <i class="icon-foot"></i>
                    </div>
                    <div data-toggle="tooltip" data-placement="top" title="Oberfläche"
                        class="pager-button button-surface">
                        <div class="bg"></div>
                        <i class="icon-surface"></i>
                    </div>
                    <div data-toggle="tooltip" data-placement="top" title="Sonstige Optionen"
                        class="pager-button button-extras">
                        <div class="bg"></div>
                        <i class="icon-extras"></i>
                    </div>
                    <div data-toggle="tooltip" data-placement="top" title="Zusatzausstattung"
                        class="pager-button button-additional">
                        <div class="bg"></div>
                        <i class="icon-additional"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-lg-4">
            </div>
        </div>
    </div>
</div>

<div class="panel-wrapper">
    <div class="container panel-container">
        <form id="configuration-form">
            <div class="option-panel measurement-panel active">
                <div class="form-row">
                    <div class="col-sm-4 col-lg-3">
                        <div class="option length">
                            <h3>Länge</h3>
                            <input type="text" name="length" value="100" class="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-3">
                        <div class="option width">
                            <h3>Breite</h3>
                            <input type="text" name="width" value="50" class="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-3">
                        <div class="option height">
                            <h3>Höhe</h3>
                            <input type="text" name="height" value="50" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="option-information">
                    <p>Diese Option ermöglicht die Anpassung der Außenmaße des Troges. Die Angaben sind in
                        Zentimeter.<br /><br />ACHTUNG: Abhängig von der Länge und den statischen Eigenschaften eines
                        Troges wird bei Bedarf automatisch ein Mittelsteg eingefügt beziehungsweise die Wandstärke
                        angepasst.</p>
                </div>
            </div>

            <div class="option-panel wall-panel">
                <div class="form-row">
                    <div class="col-sm-4">
                        <div class="option wall-thickness">
                            <h3>Wandstärke</h3>
                            <ul>
                                <li>
                                    <input type="radio" name="wall-thickness" id="wall16" value="16" checked />
                                    <label for="wall16">16 mm</label>
                                </li>
                                <li>
                                    <input type="radio" name="wall-thickness" id="wall24" value="24" />
                                    <label for="wall24">24 mm</label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="option-information">
                    <p>Wählen sie die gewünschte Wandstärke des Troges aus. Dies beeinflusst das Gewicht und die
                        Belastbarkeit Ihres Troges sowie die verfügbaren Optionen im nachfolgenden Schritt<br /><br
                            />ACHTUNG: Die gewählte Länge und statischen Anforderungen des Troges können zu einer
                        automatischen Anpassung der Wandstärke auf 24 Millimeter führen.</p>
                </div>
                <div class="form-row">
                    <div class="col-sm-8">
                        <div class="option double-frame">
                            <h3>Doppio-Rand</h3>
                            <ul>
                                <li>
                                    <input type="radio" name="double-frame" id="doppio16" value="16" checked />
                                    <label for="doppio16">Standard</label>
                                </li>
                                <li>
                                    <input type="radio" name="double-frame" id="doppio24" value="24" />
                                    <label for="doppio24">24 mm</label>
                                </li>
                                <li>
                                    <input type="radio" name="double-frame" id="doppio32" value="32" />
                                    <label for="doppio32">32 mm</label>
                                </li>
                                <li>
                                    <input type="radio" name="double-frame" id="doppio40" value="40" />
                                    <label for="doppio40">40 mm</label>
                                </li>
                                <li>
                                    <input type="radio" name="double-frame" id="doppio48" value="48" />
                                    <label for="doppio48">48 mm</label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="option-information">
                    <p>Basierend auf der Wandstärke kann ein optionaler Doppelrand am oberen Rand ausgewählt werden. Der
                        Rand besitzt mindestens die doppelte Stärke der Wand und reicht von der Oberseite ungefähr 20
                        Prozent der Troghöhe nach unten.</p>
                </div>
            </div>


            <div class="option-panel foot-panel">
                <div class="option foot-height">
                    <h3>Fußhöhe</h3>
                    <div class="row">
                        <div class="col-sm-6 col-lg-3">
                            <input type="text" name="foot-height" class="form-control" value="1.6" />
                        </div>
                    </div>
                </div>
                <div class="option skirt mt-3">
                    <h3>Schürze</h3>
                    <ul>
                        <li>
                            <input type="text" name="skirt-front" value="0" class="form-control" />
                            <label for="skirt-front">Vorne</label>
                        </li>
                        <li>
                            <input type="text" name="skirt-left" value="0" class="form-control" />
                            <label for="skirt-left">Links</label>
                        </li>
                        <li>
                            <input type="text" name="skirt-back" value="0" class="form-control" />
                            <label for="skirt-back">Hinten</label>
                        </li>
                        <li>
                            <input type="text" name="skirt-right" value="0" class="form-control" />
                            <label for="skirt-right">Rechts</label>
                        </li>
                    </ul>
                </div>
                <div class="option-information">
                    <p>Wählen sie die Höhe der Füße in Zentimetern und eine optionale Schürze am unteren Rand aus</p>
                </div>
            </div>

            <div class="option-panel surface-panel">
                <div class="option surface-type">
                    <h3>Oberflächenmaterial</h3>
                    <ul>
                        <li>
                            <input type="radio" name="color-option" class="color-option color-standard"
                                value="Standardfarbe" checked />
                            <label for="farboption-standard">Standard</label>
                        </li>
                        <li>
                            <input type="radio" name="color-option" class="color-option color-ral" value="RAL-Farbe" />
                            <label for="farboption-ral">RAL</label>
                        </li>
                        <!--
                          <li>
                              <input type="radio" name="color-option" class="color-option color-ncs" value="NCS-Farbe" />
                              <label for="farboption-ncs">NCS</label>
                          <li>
                              <input type="radio" name="color-option" class="color-option color-buntsteinputz" value="Buntsteinputz" />
                              <label for="farboption-buntstein">Buntsteinputz</label>
                          </li>
                          <li>
                              <input type="radio" name="color-option" class="color-option color-reibputz" value="Reibputz" />
                              <label for="farboption-reib">Reibputz</label>
                          </li>
                          -->
                    </ul>
                </div>
                <div class="option color-specification">
                    <h3>Farbspezifikation</h3>
                    <ul class="color-specification-input color-picker-standard">
                        <li>
                            <input type="radio" name="standard-ral" id="specification-anthrazit" value="363d43"
                                checked />
                            <label for="specification-anthrazit">Anthrazit</label>
                        </li>
                        <li>
                            <input type="radio" name="standard-ral" id="specification-verkehrsgrau" value="929899" />
                            <label for="specification-verkehrsgrau">Hellgrau</label>
                        </li>
                        <li>
                            <input type="radio" name="standard-ral" id="specificatio-nweiss" value="f4f4ed" />
                            <label for="specification-weiss">Reinweiß</label>
                        </li>
                        <li>
                            <input type="radio" name="standard-ral" id="specification-terrakotta" value="be4e24" />
                            <label for="specification-terrakotta">Terrakotta</label>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col-sm-6 col-lg-4">
                            <select name="color-picker-ral"
                                class="form-control color-specification-input color-picker-ral">@include('configurator.ral-colors')</select>
                            <input type="text" name="color-picker-ncs"
                                class="form-control color-specification-input color-picker-ncs"
                                placeholder="NCS-Code" />
                            <input type="text" name="color-picker-buntsteinputz"
                                class="form-control color-specification-input color-picker-buntsteinputz"
                                placeholder="Fabrikatsnummer" />
                            <input type="text" name="color-picker-reibputz"
                                class="form-control color-specification-input color-picker-reibputz"
                                placeholder="Fabrikatsnummer" />
                        </div>
                    </div>
                </div>
                <div class="option-information">
                    <p>Wählen sie zuerst das Oberflächenmaterial und anschließend eine Farbe. Für RAL- und NCS-Farben
                        wird ein Aufschlag von 94 Euro pro Farbe und Bestellung berechnet. Der Aufschlag wird nach der
                        Menge der verbrauchten Farbe berechnet und im Warenkorb ausgewiesen.</p>
                </div>
            </div>

            <div class="option-panel extras-panel">
                <div class="option water">
                    <h3>Wasserablauf</h3>
                    <ul>
                        <li>
                            <input type="radio" name="water-option" id="entleerung" value="Völlige Entleerung"
                                checked />
                            <label for="entleerung">Entleerung</label>
                        </li>
                        <li>
                            <input type="radio" name="water-option" id="hydrodicht" value="Hydrodicht" />
                            <label for="hydrodicht">Hydrodicht</label>
                        </li>
                        <li>
                            <input type="radio" name="water-option" id="wasserspeicher" value="Wasserspeicher" />
                            <label for="speicher">Wasserspeicher</label>
                        </li>
                    </ul>
                </div>
                <div class="option-information">
                    <p>Für den Wasserabfluss des Troges kann zwischen der vollständigen Entleerung, Hydrodicht oder
                        Wasserspeicher gewählt werden.</p>
                </div>
                <div class="option border-profile">
                    <h3>Landhausprofil</h3>
                    <ul>
                        <li>
                            <input type="checkbox" name="profile-front" class="configurator-profile" value="1" />
                            <label for="profile-front">Vorne</label>
                        </li>
                        <li>
                            <input type="checkbox" name="profile-left" class="configurator-profile" value="1" />
                            <label for="profile-left">Links</label>
                        </li>
                        <li>
                            <input type="checkbox" name="profile-back" class="configurator-profile" value="1" />
                            <label for="profile-back">Hinten</label>
                        </li>
                        <li>
                            <input type="checkbox" name="profile-right" class="configurator-profile" value="1" />
                            <label for="profile-right">Rechts</label>
                        </li>
                    </ul>
                </div>
                <div class="option-information">
                    <p>Optional kann auf Wunsch ein Landhausprofil konfiguriert werden. Dieses ist für jede Seite
                        individuell und wird an den Außenseiten des Troges montiert. Varianten des Landhausprofils
                        können in der Galerie betrachtet werden.</p>
                </div>
            </div>

            <div class="option-panel additional-panel">
                <div class="option supplemental">
                    <h3>Zusatzausstattung</h3>
                    <ul>
                        <li>
                            <input type="checkbox" name="styropor" id="styropor" />
                            <label for="styropor">Styropor</label>
                        </li>
                        <li>
                            <input type="checkbox" name="leca" id="leca" />
                            <label for="leca">Leca</label>
                        </li>
                        <li>
                            <input type="checkbox" name="vlies" id="vlies" />
                            <label for="vlies">Vlies</label>
                        </li>
                    </ul>
                </div>
                <div class="option-information">
                    <p>Wählen Sie hier das Zubehör für den Trog aus. Sie haben die Wahl aus Styropor um die Innenwändes
                        des Troges zu isolieren, sowie die Möglichkeit Leca in der richtigen Menge als Befüllung für
                        Ihren Trog zu bestellen.</p>
                </div>
                <div class="option message">
                    <h3>Sonderwunsch</h3>
                    <div class="row">
                        <div class="col-12 col-md-10 col-lg-8">
                            <textarea name="non-standard" rows="5" cols="75"
                                placeholder="Sollten Sie nicht alle gewünschten Optionen vorfinden, teilen Sie uns hiermit bitte etwaige Sonderwünsche mit. Wir werden versuchen diese je nach Möglichkeit zu berücksichtigen."></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="mobile-price-box" class="d-lg-none">
    <!--
    <div class="price-product-options">
        <span class="helmet_price">
            <strong>Trog</strong>
            <span>300&nbsp;€</span>
        </span>
        <span class="paintwork_price">
            <strong>Extras</strong>
            <span>78,12&nbsp;€</span>
        </span>
    </div>
    -->
    <div class="price">{{ $_SESSION['trough-price'] }} €</div>
    <div class="price-vat"><span class="price-value">{{ $_SESSION['trough-price'] * 1.2 }}
            €</span><span class="vat-text"> inkl. MWSt</span></div>
    <div class="add-to-cart-buttons">
        <button type="button" title="Jetzt anfragen" class="btn btn-tondo mt-3">
            <span>Jetzt anfragen</span>
        </button>
    </div>
    <div class="price-extras">
        <span class="delivery-time">mind. 5 Wochen Lieferzeit</span>
        <span class="tax-details">Lieferung per Spedition</span>
    </div>
</div>

@endsection
