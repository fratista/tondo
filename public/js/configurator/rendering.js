function initializeRendering(standardStats) {
    var length = standardStats.length;
    var height = standardStats.height;
    var width = standardStats.width;
    var wT = standardStats.wallThickness;
    var footHeight = standardStats.footHeight;
    var skirtFront = standardStats.skirtFront;
    var skirtLeft = standardStats.skirtLeft;
    var skirtBack = standardStats.skirtBack;
    var skirtRight = standardStats.skirtRight;
    var dFrame = standardStats.doubleFrame;
    var profileFront = standardStats.profileFront;
    var profileLeft = standardStats.profileLeft;
    var profileBack = standardStats.profileBack;
    var profileRight = standardStats.profileRight;
    var styropor = standardStats.styropor;
    var vlies = standardStats.vlies;
    var leca = standardStats.leca;
    var color = parseInt("0x" + standardStats.colorSpecification);
    var middlePieceOption = standardStats.middlePiece;
    var rotationX = standardStats.rotationX;
    var rotationY = standardStats.rotationY;
    var zoomOffset = standardStats.zoomOffset;

    var scene, camera, light, renderer;

    var targetRotationX = 0;
    var targetRotationY = 0;
    var targetRotationOnMouseDownX = 0;
    var targetRotationOnMouseDownY = 0;

    var mouseX = 0;
    var mouseY = 0;
    var mouseXOnMouseDown = 0;
    var mouseYOnMouseDown = 0;

    var windowHalfX = window.innerWidth / 2;
    var windowHalfY = window.innerHeight / 2;
    var defaultDeviceHeight = 2160;
    var deviceHeight = window.screen.height;
    var scalingFactor = defaultDeviceHeight / deviceHeight + 0.2;
    var cameraPosition;

    if (length >= 125) cameraPosition = -length / 30;
    else if (length >= 145) cameraPosition = -length / 15;
    else if (length >= 170) cameraPosition = -length / 12.5;
    else if (length >= 185) cameraPosition = -length / 10;
    else cameraPosition = 0;

    var zoom = 50 + zoomOffset;
    if (length >= 140 && length < 155)
        if (zoom < 52.5) zoom = 52.5;
    if (length >= 160 && length < 175)
        if (zoom < 55) zoom = 55;
    if (length >= 175 && length < 190)
        if (zoom < 57.5) zoom = 57.5;
    if (length >= 190)
        if (zoom < 115) zoom = 60;

    if (height >= 85)
        if (rotationX < -0.1) rotationX = -0.1;

    prepareRendering();

    /**
     * Pass the input to the rendering engine
     *
     */
    function prepareRendering() {
        scene = new THREE.Scene();

        /*camera = (window.innerWidth < window.innerHeight) ? new THREE.PerspectiveCamera(zoom + 30, window.innerWidth / window.innerHeight, 0.1, 1000) : new THREE.PerspectiveCamera(zoom, 2, 0.1, 1000);*/
        camera = new THREE.PerspectiveCamera(zoom, 2, 0.1, 1000);
        camera.position.set(30, height * 1.6, 145);
        camera.lookAt(new THREE.Vector3(cameraPosition, height / 5, 0));

        light = new THREE.DirectionalLight(0xFFFFFF, 1);
        light.position.set(0, 90, 0);
        light.castShadow = true;
        light.shadowDarkness = 0.9;
        scene.add(light);
        scene.add(new THREE.AmbientLight(0xFFFFFF));

        renderer = new THREE.WebGLRenderer({
            antialias: true,
            alpha: true
        });
        renderer.setClearColor(0xD1D1D1, 0);
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(window.innerWidth, window.innerHeight);
        renderer.shadowMap.Enabled = true;
        renderer.shadowMapSoft = true;
        $("#rendering").append(renderer.domElement);

        //var textureLoader = new THREE.TextureLoader();
        //textureLoader.load('../img/home/configurator.png', function(texture)  {
        //    var troughMaterial = new THREE.MeshBasicMaterial({map: texture});
        //});

        /**********Geometrische Daten Hülle des Troges**********/
        var outerShellGeometry = new THREE.Geometry();
        var innerShellGeometry = new THREE.Geometry();

        //Vektoren für Eckpunkte definieren
        outerShellGeometry.vertices.push(
            new THREE.Vector3(0, 0, 0),
            new THREE.Vector3(length, 0, 0),
            new THREE.Vector3(0, height, 0),
            new THREE.Vector3(length, height, 0),
            new THREE.Vector3(0, 0, -width),
            new THREE.Vector3(0, height, -width),
            new THREE.Vector3(length, 0, -width),
            new THREE.Vector3(length, height, -width)
        );

        innerShellGeometry.vertices.push(
            new THREE.Vector3(wT, wT, 0 - wT),
            new THREE.Vector3(length - wT, wT, 0 - wT),
            new THREE.Vector3(wT, height, 0 - wT),
            new THREE.Vector3(length - wT, height, 0 - wT),
            new THREE.Vector3(wT, wT, -width),
            new THREE.Vector3(wT, height, -width + wT),
            new THREE.Vector3(length - wT, wT, -width + wT),
            new THREE.Vector3(length - wT, height, -width + wT)
        );

        outerShell = createShell(outerShellGeometry, color);
        innerShell = createShell(innerShellGeometry, 0x636363);

        /**********Geometrische Daten Mittelsteg**********/
        if (middlePieceOption === true && (length != width)) {
            var middlePieceGeometry = new THREE.Geometry();

            middlePieceGeometry.vertices.push(
                new THREE.Vector3(length / 2 - wT, wT + 5, -wT),
                new THREE.Vector3(length / 2 - wT, wT + 5, -width + wT),
                new THREE.Vector3(length / 2 - wT, height - 5, -width + wT),
                new THREE.Vector3(length / 2 - wT, height - 5, -wT),
                new THREE.Vector3(length / 2 + wT, wT + 5, -wT),
                new THREE.Vector3(length / 2 + wT, wT + 5, -width + wT),
                new THREE.Vector3(length / 2 + wT, height - 5, -width + wT),
                new THREE.Vector3(length / 2 + wT, height - 5, -wT)
            );

            middlePiece = createMiddlePiece(middlePieceGeometry);
        } else middlePiece = 0;

        /**********Geometrische Daten oberer Rand**********/
        var wallToppingGeometry = new THREE.Geometry();

        wallToppingGeometry.vertices.push(
            new THREE.Vector3(0, height, 0),
            new THREE.Vector3(length, height, 0),
            new THREE.Vector3(0, height, -width),
            new THREE.Vector3(length, height, -width),
            new THREE.Vector3(wT, height, 0 - wT),
            new THREE.Vector3(length - wT, height, 0 - wT),
            new THREE.Vector3(wT, height, -width + wT),
            new THREE.Vector3(length - wT, height, -width + wT),

            //Hilfspunkte
            new THREE.Vector3(wT, height, 0),
            new THREE.Vector3(length, height, 0 - wT),
            new THREE.Vector3(length - wT, height, -width),
            new THREE.Vector3(wT, height, -width)
        );

        wallToppingGeometry.faces.push(
            new THREE.Face3(1, 8, 9), new THREE.Face3(9, 8, 4), //vorderer Rand
            new THREE.Face3(0, 2, 8), new THREE.Face3(11, 8, 2), //linker Rand
            new THREE.Face3(11, 10, 6), new THREE.Face3(6, 10, 7), //hinterer Rand
            new THREE.Face3(5, 10, 9), new THREE.Face3(3, 9, 10) //rechter Rand
        );

        wallTopping = finishMesh(wallToppingGeometry, color);

        /**********Geometrische Daten Füße**********/
        var additionalFootCount = 0;
        var remainingLength = length - 30;

        var feetGeometry = new THREE.Geometry();

        feetGeometry.vertices.push(
            //linker Fuß
            new THREE.Vector3(5, 0, -5),
            new THREE.Vector3(5, 0, -width + 5),
            new THREE.Vector3(15, 0, -width + 5),
            new THREE.Vector3(15, 0, -5),
            //rechter Fuß
            new THREE.Vector3(length - 5, 0, -5),
            new THREE.Vector3(length - 5, 0, -width + 5),
            new THREE.Vector3(length - 15, 0, -width + 5),
            new THREE.Vector3(length - 15, 0, -5),
            //linker Fuß Boden
            new THREE.Vector3(5, -footHeight, -5),
            new THREE.Vector3(5, -footHeight, -width + 5),
            new THREE.Vector3(15, -footHeight, -width + 5),
            new THREE.Vector3(15, -footHeight, -5),
            //rechter Fuß Boden
            new THREE.Vector3(length - 5, -footHeight, -5),
            new THREE.Vector3(length - 5, -footHeight, -width + 5),
            new THREE.Vector3(length - 15, -footHeight, -width + 5),
            new THREE.Vector3(length - 15, -footHeight, -5)
        );

        feetGeometry.faces.push(
            //linker Fuß
            new THREE.Face3(0, 1, 3), new THREE.Face3(3, 1, 2),
            new THREE.Face3(8, 9, 11), new THREE.Face3(11, 9, 10),
            new THREE.Face3(0, 3, 8), new THREE.Face3(8, 3, 11),
            new THREE.Face3(0, 1, 9), new THREE.Face3(9, 1, 8),
            new THREE.Face3(1, 2, 9), new THREE.Face3(9, 2, 10),
            new THREE.Face3(10, 11, 3), new THREE.Face3(3, 10, 2),
            //rechter Fuß
            new THREE.Face3(4, 5, 6), new THREE.Face3(7, 4, 6),
            new THREE.Face3(12, 13, 14), new THREE.Face3(14, 15, 12),
            new THREE.Face3(4, 7, 12), new THREE.Face3(12, 7, 15),
            new THREE.Face3(4, 5, 13), new THREE.Face3(13, 5, 12),
            new THREE.Face3(5, 6, 13), new THREE.Face3(13, 6, 14),
            new THREE.Face3(14, 15, 7), new THREE.Face3(7, 14, 6)
        );

        while (remainingLength > 40) {
            additionalFootCount++;
            remainingLength -= 50;
        }

        var footMidpoint = (length - 30) / (additionalFootCount + 1);

        for (var i = 1; i <= additionalFootCount; i++) {
            //Bisherige Anzahl der Vektoren bestimmen
            var verticeIndex = feetGeometry.vertices.length;

            feetGeometry.vertices.push(
                new THREE.Vector3(15 + i * footMidpoint - 5, 0, -5),
                new THREE.Vector3(15 + i * footMidpoint - 5, 0, -width + 5),
                new THREE.Vector3(15 + i * footMidpoint + 5, 0, -width + 5),
                new THREE.Vector3(15 + i * footMidpoint + 5, 0, -5),
                new THREE.Vector3(15 + i * footMidpoint - 5, -footHeight, -5),
                new THREE.Vector3(15 + i * footMidpoint - 5, -footHeight, -width + 5),
                new THREE.Vector3(15 + i * footMidpoint + 5, -footHeight, -width + 5),
                new THREE.Vector3(15 + i * footMidpoint + 5, -footHeight, -5)
            );

            feetGeometry.faces.push(
                new THREE.Face3(verticeIndex, verticeIndex + 1, verticeIndex + 2),
                new THREE.Face3(verticeIndex + 2, verticeIndex + 3, verticeIndex),
                new THREE.Face3(verticeIndex + 4, verticeIndex + 5, verticeIndex + 6),
                new THREE.Face3(verticeIndex + 6, verticeIndex + 7, verticeIndex + 4),
                new THREE.Face3(verticeIndex, verticeIndex + 3, verticeIndex + 4),
                new THREE.Face3(verticeIndex + 4, verticeIndex + 3, verticeIndex + 7),
                new THREE.Face3(verticeIndex, verticeIndex + 4, verticeIndex + 5),
                new THREE.Face3(verticeIndex + 5, verticeIndex, verticeIndex + 1),
                new THREE.Face3(verticeIndex + 1, verticeIndex + 5, verticeIndex + 2),
                new THREE.Face3(verticeIndex + 2, verticeIndex + 5, verticeIndex + 6),
                new THREE.Face3(verticeIndex + 6, verticeIndex + 7, verticeIndex + 3),
                new THREE.Face3(verticeIndex + 3, verticeIndex + 6, verticeIndex + 2)
            );
        }

        feet = finishMesh(feetGeometry, 0x858585);

        /**********Geometrische Daten Schürze**********/
        var skirtGeometry = new THREE.Geometry();

        if (skirtFront > 0) {
            skirtGeometry.vertices.push(
                new THREE.Vector3(0, 0, 0),
                new THREE.Vector3(length, 0, 0),
                new THREE.Vector3(length, -skirtFront, 0),
                new THREE.Vector3(0, -skirtFront, 0),
                new THREE.Vector3(0, 0, -wT),
                new THREE.Vector3(length, 0, -wT),
                new THREE.Vector3(length, -skirtFront, -wT),
                new THREE.Vector3(0, -skirtFront, -wT)
            );
            var vIndex = skirtGeometry.vertices.length;
            createSkirt(skirtGeometry, vIndex - 8);
        }

        if (skirtLeft > 0) {
            skirtGeometry.vertices.push(
                new THREE.Vector3(0, 0, -width),
                new THREE.Vector3(0, 0, 0),
                new THREE.Vector3(0, -skirtLeft, 0),
                new THREE.Vector3(0, -skirtLeft, -width),
                new THREE.Vector3(wT, 0, -width),
                new THREE.Vector3(wT, 0, 0),
                new THREE.Vector3(wT, -skirtLeft, 0),
                new THREE.Vector3(wT, -skirtLeft, -width)
            );
            var vIndex = skirtGeometry.vertices.length;
            createSkirt(skirtGeometry, vIndex - 8);
        }

        if (skirtBack > 0) {
            skirtGeometry.vertices.push(
                new THREE.Vector3(0, 0, -width),
                new THREE.Vector3(length, 0, -width),
                new THREE.Vector3(length, -skirtBack, -width),
                new THREE.Vector3(0, -skirtBack, -width),
                new THREE.Vector3(0, 0, -width + wT),
                new THREE.Vector3(length, 0, -width + wT),
                new THREE.Vector3(length, -skirtBack, -width + wT),
                new THREE.Vector3(0, -skirtBack, -width + wT)
            );

            var vIndex = skirtGeometry.vertices.length;
            createSkirt(skirtGeometry, vIndex - 8);
        }

        if (skirtRight > 0) {
            skirtGeometry.vertices.push(
                new THREE.Vector3(length, 0, 0),
                new THREE.Vector3(length, 0, -width),
                new THREE.Vector3(length, -skirtRight, -width),
                new THREE.Vector3(length, -skirtRight, 0),
                new THREE.Vector3(length - wT, 0, 0),
                new THREE.Vector3(length - wT, 0, -width),
                new THREE.Vector3(length - wT, -skirtRight, -width),
                new THREE.Vector3(length - wT, -skirtRight, 0)
            );

            var vIndex = skirtGeometry.vertices.length;
            createSkirt(skirtGeometry, vIndex - 8);
        }

        skirt = finishMesh(skirtGeometry, color);

        //Geometrische Daten Doppio-Rand
        if (dFrame > 2.4) {
            var doubleFrameGeometry = new THREE.Geometry();

            doubleFrameGeometry.vertices.push(
                //Punkte Innenhülle oberer Rand
                new THREE.Vector3(wT, height, -wT),
                new THREE.Vector3(length - wT, height, -wT),
                new THREE.Vector3(length - wT, height, -width + wT),
                new THREE.Vector3(wT, height, -width + wT),
                //Innenhülle-Endpunkte vertikal (90 Prozent der Troghöhe)
                new THREE.Vector3(wT, height * 0.66, -wT),
                new THREE.Vector3(length - wT, height * 0.66, -wT),
                new THREE.Vector3(length - wT, height * 0.66, -width + wT),
                new THREE.Vector3(wT, height * 0.66, -width + wT),
                //Doppiopunkte oberer Rand
                new THREE.Vector3(dFrame, height, -dFrame),
                new THREE.Vector3(length - dFrame, height, -dFrame),
                new THREE.Vector3(length - dFrame, height, -width + dFrame),
                new THREE.Vector3(dFrame, height, -width + dFrame),
                //Doppiopunkte vertikal
                new THREE.Vector3(dFrame, height * 0.66, -dFrame),
                new THREE.Vector3(length - dFrame, height * 0.66, -dFrame),
                new THREE.Vector3(length - dFrame, height * 0.66, -width + dFrame),
                new THREE.Vector3(dFrame, height * 0.66, -width + dFrame)
            );

            doubleFrameGeometry.faces.push(
                //oberer Doppio-Rand
                new THREE.Face3(1, 10, 2), new THREE.Face3(1, 9, 10),
                new THREE.Face3(0, 11, 8), new THREE.Face3(0, 3, 11),
                new THREE.Face3(2, 11, 3), new THREE.Face3(2, 10, 11),
                new THREE.Face3(1, 8, 9), new THREE.Face3(8, 1, 0),
                //Doppio-Körper
                new THREE.Face3(8, 13, 12), new THREE.Face3(8, 9, 13),
                new THREE.Face3(8, 15, 11), new THREE.Face3(8, 12, 15),
                new THREE.Face3(10, 15, 11), new THREE.Face3(10, 14, 15),
                new THREE.Face3(9, 14, 10), new THREE.Face3(9, 13, 14),
                //unterer Doppio-Rand
                new THREE.Face3(4, 12, 15), new THREE.Face3(15, 4, 7),
                new THREE.Face3(7, 15, 6), new THREE.Face3(6, 15, 14),
                new THREE.Face3(14, 6, 5), new THREE.Face3(5, 14, 13),
                new THREE.Face3(5, 13, 12), new THREE.Face3(12, 5, 4)
            );

            doubleFrame = finishMesh(doubleFrameGeometry, color);
        } else doubleFrame = 0;

        //Geometrische Daten Landhausprofil
        if (profileFront === true) {
            var borderProfileFrontGeometry = new THREE.Geometry();

            borderProfileFrontGeometry.vertices.push(
                new THREE.Vector3(0, height, 0),
                new THREE.Vector3(length, height, 0),
                new THREE.Vector3(length, height - 6.5, 0),
                new THREE.Vector3(0, height - 6.5, 0),
                new THREE.Vector3(0, height, 1.5),
                new THREE.Vector3(length, height, 1.5),
                new THREE.Vector3(length, height - 6.5, 1.5),
                new THREE.Vector3(0, height - 6.5, 1.5),
                //Teilstück
                new THREE.Vector3(0, height - 2.5, 2),
                new THREE.Vector3(length, height - 2.5, 2),
                new THREE.Vector3(length, height - 4.5, 2),
                new THREE.Vector3(0, height - 4.5, 2),
                new THREE.Vector3(0, height - 2.5, 1.5),
                new THREE.Vector3(length, height - 2.5, 1.5),
                new THREE.Vector3(length, height - 4.5, 1.5),
                new THREE.Vector3(0, height - 4.5, 1.5)
            );
            borderProfileFront = createBorderProfile(new THREE.LineCurve3(new THREE.Vector3(0, height - 1.3, 2), new THREE.Vector3(length, height - 1.3, 2)), borderProfileFrontGeometry);
        } else borderProfileFront = 0;

        if (profileLeft === true) {
            var borderProfileLeftGeometry = new THREE.Geometry();

            borderProfileLeftGeometry.vertices.push(
                new THREE.Vector3(0, height, -width),
                new THREE.Vector3(0, height, 0),
                new THREE.Vector3(0, height - 6.5, 0),
                new THREE.Vector3(0, height - 6.5, -width),
                new THREE.Vector3(-1.5, height, -width),
                new THREE.Vector3(-1.5, height, 0),
                new THREE.Vector3(-1.5, height - 6.5, 0),
                new THREE.Vector3(-1.5, height - 6.5, -width),
                //Teilstück
                new THREE.Vector3(-2, height - 2.5, -width),
                new THREE.Vector3(-2, height - 2.5, 0),
                new THREE.Vector3(-2, height - 4.5, 0),
                new THREE.Vector3(-2, height - 4.5, -width),
                new THREE.Vector3(-1.5, height - 2.5, -width),
                new THREE.Vector3(-1.5, height - 2.5, 0),
                new THREE.Vector3(-1.5, height - 4.5, 0),
                new THREE.Vector3(-1.5, height - 4.5, -width)
            );

            borderProfileLeft = createBorderProfile(new THREE.LineCurve3(new THREE.Vector3(-2, height - 1.3, 0), new THREE.Vector3(-2, height - 1.3, -width)), borderProfileLeftGeometry);
        } else borderProfileLeft = 0;


        if (profileBack === true) {
            var borderProfileBackGeometry = new THREE.Geometry();

            borderProfileBackGeometry.vertices.push(
                new THREE.Vector3(length, height, -width),
                new THREE.Vector3(0, height, -width),
                new THREE.Vector3(0, height - 6.5, -width),
                new THREE.Vector3(length, height - 6.5, -width),
                new THREE.Vector3(length, height, -width - 1.5),
                new THREE.Vector3(0, height, -width - 1.5),
                new THREE.Vector3(0, height - 6.5, -width - 1.5),
                new THREE.Vector3(length, height - 6.5, -width - 1.5),
                //Teilstück
                new THREE.Vector3(length, height - 2.5, -width - 2),
                new THREE.Vector3(0, height - 2.5, -width - 2),
                new THREE.Vector3(0, height - 4.5, -width - 2),
                new THREE.Vector3(length, height - 4.5, -width - 2),
                new THREE.Vector3(length, height - 2.5, -width - 1.5),
                new THREE.Vector3(0, height - 2.5, -width - 1.5),
                new THREE.Vector3(0, height - 4.5, -width - 1.5),
                new THREE.Vector3(length, height - 4.5, -width - 1.5)
            );

            borderProfileBack = createBorderProfile(new THREE.LineCurve3(new THREE.Vector3(0, height - 1.3, -width - 2), new THREE.Vector3(length, height - 1.3, -width - 2)), borderProfileBackGeometry);
        } else borderProfileBack = 0;

        if (profileRight === true) {
            var borderProfileRightGeometry = new THREE.Geometry();

            borderProfileRightGeometry.vertices.push(
                new THREE.Vector3(length, height, 0),
                new THREE.Vector3(length, height, -width),
                new THREE.Vector3(length, height - 6.5, -width),
                new THREE.Vector3(length, height - 6.5, 0),
                new THREE.Vector3(length + 1.5, height, 0),
                new THREE.Vector3(length + 1.5, height, -width),
                new THREE.Vector3(length + 1.5, height - 6.5, -width),
                new THREE.Vector3(length + 1.5, height - 6.5, 0),
                //Teilstück
                new THREE.Vector3(length + 2, height - 2.5, 0),
                new THREE.Vector3(length + 2, height - 2.5, -width),
                new THREE.Vector3(length + 2, height - 4.5, -width),
                new THREE.Vector3(length + 2, height - 4.5, 0),
                new THREE.Vector3(length + 1.5, height - 2.5, 0),
                new THREE.Vector3(length + 1.5, height - 2.5, -width),
                new THREE.Vector3(length + 1.5, height - 4.5, -width),
                new THREE.Vector3(length + 1.5, height - 4.5, 0)
            );

            borderProfileRight = createBorderProfile(new THREE.LineCurve3(new THREE.Vector3(length + 2, height - 1.3, 0), new THREE.Vector3(length + 2, height - 1.3, -width)), borderProfileRightGeometry);
        } else borderProfileRight = 0;

        //Geometrische Daten Styropor
        if (styropor === true) {
            if (dFrame <= 2.4) var styroporThickness = 3.2;
            else var styroporThickness = dFrame;

            var styroporGeometry = new THREE.Geometry();

            styroporGeometry.vertices.push(
                new THREE.Vector3(length - wT, height * 0.66, -wT),
                new THREE.Vector3(wT, height * 0.66, -wT),
                new THREE.Vector3(wT, wT, -wT),
                new THREE.Vector3(length - wT, wT, -wT),
                new THREE.Vector3(length - wT, height * 0.66, -styroporThickness),
                new THREE.Vector3(wT, height * 0.66, -styroporThickness),
                new THREE.Vector3(wT, wT, -styroporThickness),
                new THREE.Vector3(length - wT, wT, -styroporThickness),
                new THREE.Vector3(wT, height * 0.66, -wT),
                new THREE.Vector3(wT, height * 0.66, -width + wT),
                new THREE.Vector3(wT, wT, -width + wT),
                new THREE.Vector3(wT, wT, -wT),
                new THREE.Vector3(styroporThickness, height * 0.66, -wT),
                new THREE.Vector3(styroporThickness, height * 0.66, -width + wT),
                new THREE.Vector3(styroporThickness, wT, -width + wT),
                new THREE.Vector3(styroporThickness, wT, -wT),
                new THREE.Vector3(wT, height * 0.66, -width + wT),
                new THREE.Vector3(length - wT, height * 0.66, -width + wT),
                new THREE.Vector3(length - wT, wT, -width + wT),
                new THREE.Vector3(wT, wT, -width + wT),
                new THREE.Vector3(wT, height * 0.66, -width + styroporThickness),
                new THREE.Vector3(length - wT, height * 0.66, -width + styroporThickness),
                new THREE.Vector3(length - wT, wT, -width + styroporThickness),
                new THREE.Vector3(wT, wT, -width + styroporThickness),
                new THREE.Vector3(length - wT, height * 0.66, -width + wT),
                new THREE.Vector3(length - wT, height * 0.66, -wT),
                new THREE.Vector3(length - wT, wT, -wT),
                new THREE.Vector3(length - wT, wT, -width + wT),
                new THREE.Vector3(length - styroporThickness, height * 0.66, -width + wT),
                new THREE.Vector3(length - styroporThickness, height * 0.66, -wT),
                new THREE.Vector3(length - styroporThickness, wT, -wT),
                new THREE.Vector3(length - styroporThickness, wT, -width + wT),
                new THREE.Vector3(wT, wT, -width + wT),
                new THREE.Vector3(length - wT, wT, -width + wT),
                new THREE.Vector3(length - wT, wT, -wT),
                new THREE.Vector3(wT, wT, -wT),
                new THREE.Vector3(wT, styroporThickness, -width + wT),
                new THREE.Vector3(length - wT, styroporThickness, -width + wT),
                new THREE.Vector3(length - wT, styroporThickness, -wT),
                new THREE.Vector3(wT, styroporThickness, -wT)
            );

            for (var i = 0; i <= 32; i += 8) {
                styroporGeometry.faces.push(
                    //new THREE.Face3(4 + i, 5 + i, 6 + i), new THREE.Face3(6 + i, 4 + i, 7 + i)
                    //new THREE.Face3(4 + i, 0 + i, 3 + i), new THREE.Face3(3 + i, 4 + i, 7 + i)
                    new THREE.Face3(4 + i, 0 + i, 1 + i), new THREE.Face3(1 + i, 4 + i, 5 + i), //Oberrand
                    new THREE.Face3(1 + i, 5 + i, 2 + i), new THREE.Face3(5 + i, 2 + i, 6 + i)
                );
            }

            styropor = finishMesh(styroporGeometry, 0x9C9C9C);
        } else styropor = 0;

        getImageData = true;
        render();
    }

    function onWindowResize() {
        windowHalfX = window.innerWidth / 2;
        windowHalfY = window.innerHeight / 2;

        camera.aspect = window.innerWidth / (window.innerHeight * 1.8);
        camera.updateProjectionMatrix();

        renderer.setSize(window.innerWidth, window.innerHeight);
    }

    function onDocumentMouseDown(event) {
        event.preventDefault();

        document.addEventListener('mousemove', onDocumentMouseMove, false);
        document.addEventListener('mouseup', onDocumentMouseUp, false);
        document.addEventListener('mouseout', onDocumentMouseOut, false);

        mouseXOnMouseDown = event.clientX - windowHalfX;
        mouseYOnMouseDown = event.clientY - windowHalfY;
        targetRotationOnMouseDownX = targetRotationX;
        targetRotationOnMouseDownY = targetRotationY;
    }

    function onDocumentMouseMove(event) {
        mouseX = event.clientX - windowHalfX;
        mouseY = event.clientY - windowHalfY;
        targetRotationX = targetRotationOnMouseDownX + (mouseY - mouseYOnMouseDown) * 0.02;
        targetRotationY = targetRotationOnMouseDownY + (mouseX - mouseXOnMouseDown) * 0.02;
    }

    function onDocumentMouseWheel(event) {
        var e = window.event || e; // old IE support
        var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));

        if ((camera.zoom += delta * 0.03) > 3.4) camera.zoom = 3.4;
        else if ((camera.zoom += delta * 0.03) < 0.5) camera.zoom = 0.5;

        camera.updateProjectionMatrix();
    }

    function onDocumentMouseUp(event) {
        document.removeEventListener('mousemove', onDocumentMouseMove, false);
        document.removeEventListener('mouseup', onDocumentMouseUp, false);
        document.removeEventListener('mouseout', onDocumentMouseOut, false);
    }

    function onDocumentMouseOut(event) {
        document.removeEventListener('mousemove', onDocumentMouseMove, false);
        document.removeEventListener('mouseup', onDocumentMouseUp, false);
        document.removeEventListener('mouseout', onDocumentMouseOut, false);
    }

    function onDocumentTouchStart(event) {
        if (event.touches.length === 1) {
            event.preventDefault();

            mouseXOnMouseDown = event.touches[0].pageX - windowHalfX;
            mouseYOnMouseDown = event.touches[0].pageY - windowHalfY;
            targetRotationOnMouseDownX = targetRotationX;
            targetRotationOnMouseDownY = targetRotationY;
        }
    }

    function onDocumentTouchMove(event) {
        if (event.touches.length === 1) {
            event.preventDefault();

            mouseX = event.touches[0].pageX - windowHalfX;
            mouseY = event.touches[0].pageY - windowHalfY;
            targetRotationX = targetRotationOnMouseDownX + (mouseY - mouseXOnMouseDown) * 0.05
            targetRotationY = targetRotationOnMouseDownY + (mouseX - mouseXOnMouseDown) * 0.05;
        }
    }

    function setFov(fov) {
        camera.fov = fov;
        camera.updateProjectionMatrix();
    }

    function setZoom(zoom) {
        camera.zoom = zoom;
        camera.updateProjectionMatrix();
    }

    function rotateMesh(axis) {
        for (var i = 0; i < meshes.length; i++)
            if (meshes[i] !== undefined) meshes[i].rotation.axis += 0.1;
    }

    function createShell(shellGeometry, color) {
        shellGeometry.faces.push(
            new THREE.Face3(0, 1, 2), new THREE.Face3(1, 3, 2), //Vorderseite
            new THREE.Face3(0, 4, 2), new THREE.Face3(4, 5, 2), //linke Seite
            new THREE.Face3(4, 5, 6), new THREE.Face3(5, 7, 6), //Rückseite
            new THREE.Face3(7, 6, 1), new THREE.Face3(1, 3, 7), //rechte Seite
            new THREE.Face3(0, 1, 4), new THREE.Face3(1, 6, 4) //Unterseite
        );

        var shell = finishMesh(shellGeometry, color);
        return shell;
    }

    function createMiddlePiece(middlePieceGeometry) {
        middlePieceGeometry.faces.push(
            new THREE.Face3(0, 1, 2), new THREE.Face3(2, 0, 3),
            new THREE.Face3(2, 3, 6), new THREE.Face3(6, 3, 7),
            new THREE.Face3(4, 5, 6), new THREE.Face3(4, 6, 7)
        );

        var middlePiece = finishMesh(middlePieceGeometry, color);
        return middlePiece;
    }

    function createSkirt(skirtGeometry, vIndex) {
        skirtGeometry.faces.push(
            new THREE.Face3(vIndex, vIndex + 1, vIndex + 2), new THREE.Face3(vIndex, vIndex + 2, vIndex + 3),
            new THREE.Face3(vIndex, vIndex + 3, vIndex + 4), new THREE.Face3(vIndex, vIndex + 4, vIndex + 7),
            new THREE.Face3(vIndex + 7, vIndex + 6, vIndex + 4), new THREE.Face3(vIndex + 6, vIndex + 4, vIndex + 2),
            new THREE.Face3(vIndex + 1, vIndex + 5, vIndex + 6), new THREE.Face3(vIndex + 6, vIndex + 2, vIndex + 1),
            new THREE.Face3(vIndex + 7, vIndex + 3, vIndex + 2), new THREE.Face3(vIndex + 2, vIndex + 6, vIndex + 7)
        );
    }

    function createBorderProfile(lineCurve, profileGeometry) {
        var profileCylinderGeometry = new THREE.TubeGeometry(lineCurve, 64, 1.2, 40, false);

        profileGeometry.faces.push(
            new THREE.Face3(0, 1, 2), new THREE.Face3(2, 0, 3),
            new THREE.Face3(4, 5, 6), new THREE.Face3(6, 4, 7),
            new THREE.Face3(0, 4, 3), new THREE.Face3(4, 3, 7),
            new THREE.Face3(1, 2, 5), new THREE.Face3(2, 5, 6),
            new THREE.Face3(0, 1, 5), new THREE.Face3(5, 0, 4),
            new THREE.Face3(3, 2, 6), new THREE.Face3(6, 3, 7),
            new THREE.Face3(8, 9, 10), new THREE.Face3(10, 8, 11),
            new THREE.Face3(8, 11, 12), new THREE.Face3(11, 12, 15),
            new THREE.Face3(9, 10, 13), new THREE.Face3(13, 10, 14),
            new THREE.Face3(14, 10, 11), new THREE.Face3(11, 10, 15)
        );

        profileCylinderGeometry.merge(profileGeometry);
        var profileSideBox = finishMesh(profileCylinderGeometry, 0xC9C9C9);
        return profileSideBox;
    }

    function finishMesh(geometry, color) {
        var material = new THREE.MeshPhongMaterial({
            color: color
        });
        material.shininess = 0;

        geometry.computeFaceNormals();
        geometry.computeVertexNormals();
        geometry.translate(-length / 2, 0, width / 2);
        geometry.verticesNeedUpdate = true;
        geometry.dynamic = true;

        var mesh = new THREE.Mesh(geometry, material);
        mesh.material.side = THREE.DoubleSide;
        mesh.receiveShadow = true;
        mesh.castShadow = true;
        scene.add(mesh);

        return mesh;
    }

    function render() {
        var meshes = [
            outerShell,
            innerShell,
            middlePiece,
            wallTopping,
            feet,
            skirt,
            doubleFrame,
            borderProfileFront,
            borderProfileLeft,
            borderProfileBack,
            borderProfileRight,
            styropor
        ];

        for (var i = 0; i < meshes.length; i++) {
            if (typeof meshes[i] !== 'undefined' && typeof meshes[i] !== 'number') {
                meshes[i].rotation.x = rotationX;
                meshes[i].rotation.y = rotationY;
                meshes[i].rotation.x += (targetRotationX - meshes[i].rotation.x) * 0.1;
                meshes[i].rotation.y += (targetRotationY - meshes[i].rotation.y) * 0.1;
            }
        }

        requestAnimationFrame(render);
        renderer.render(scene, camera);

        if (getImageData === true) {
            imgData = renderer.domElement.toDataURL();
            getImageData = false;
        }

        setTimeout(function() {
            $(".modal, .modal-container.information").removeClass('in');
            //$('.configurator-footer').find('input, textarea, button, select').prop('disabled', false);
        }, 500);
    }

    $('#rendering').on('mousedown', onDocumentMouseDown);
    $('#rendering').on('mousewheel', onDocumentMouseWheel);
    $('#rendering').on('touchstart', onDocumentTouchStart);
    $('#rendering').on('touchmove', onDocumentTouchMove);
    //window.addEventListener('resize', onWindowResize, false);
    window.THREE = THREE;
}