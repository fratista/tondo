/**
 * Initialize the configurator.
 *
 *
 */
function initializeConfigurator() {
    /**
     * Hide the initial doppio values.
     *
     *
     */
    $("#doppio24, label[for=doppio24], #doppio40, label[for=doppio40]")
        .css("display", "none")
        .prop("disabled", true);

    colorPicker();

    /**
     * Initial rendering and calculation of the trough.
     *
     *
     */
    renderTrough();
    calculateTrough();

    $(".btn.request-offer").prop("disabled", true);

    $("#wall16, #wall24")
        .off("click")
        .on("click", function() {
            switchWall(this);
        });

    $(".pager-button")
        .off("click")
        .on("click", function() {
            switchPanels(this);
        });

    $("#configuration-form")
        .off("change")
        .on("change", function() {
            calculateTrough();
        });

    $("#rendering-controls .control")
        .off("click")
        .on("click", function() {
            cameraOptions();
        });

    $("#request-form input")
        .off("keydown")
        .on("keydown", function() {
            requestReady();
        });

    $("#request-modal .request-offer")
        .off("click")
        .on("click", function() {
            sendOffer();
        });
}

/**
 * Validate values before calculation.
 *
 *
 */
function validateMeasures(input, minValue, maxValue) {
    if (
        Number($("input[name=" + input + "]").val() >= minValue) &&
        Number($("input[name=" + input + "]").val() <= maxValue)
    ) {
        input = Number($("input[name=" + input + "]").val());
    } else if (Number($("input[name=" + input + "]").val() <= minValue)) {
        $("input[name=" + input + "]").val(minValue);
        input = Number($("input[name=" + input + "]").val());
    } else if (Number($("input[name=" + input + "]").val() >= maxValue)) {
        $("input[name=" + input + "]").val(maxValue);
        input = Number($("input[name=" + input + "]").val());
    }

    return input;
}

/**
 * Switch wall strength and call method to switch doppio strength as well.
 *
 *
 */
function switchWall(element) {
    var thickness = $(element).val();
    var doppio = validateDoppio(thickness);
}

/**
 * Change doppio.
 *
 *
 */
function validateDoppio(thickness) {
    var opposite = thickness == 16 ? 24 : 16;
    var doppio = thickness == 16 ? 32 : 40;
    var doppioOpposite = thickness == 16 ? 40 : 32;

    $("label[for=doppio" + thickness + "]").html("Standard");
    $("label[for=doppio" + opposite + "]").html(opposite + " mm");
    $(
        "#doppio" +
            opposite +
            ", #doppio" +
            opposite +
            " + label, #doppio" +
            doppioOpposite +
            ", #doppio" +
            doppioOpposite +
            " + label"
    )
        .css("display", "none")
        .prop("disabled", false);
    $(
        "#doppio" +
            thickness +
            ", #doppio" +
            thickness +
            " + label, #doppio" +
            doppio +
            ", #doppio" +
            doppio +
            " + label"
    )
        .css("display", "block")
        .prop("disabled", false);

    $("#doppio" + opposite).prop("checked", false);
    $("#doppio" + thickness).prop("checked", true);
}

/**
 * Switch panels for configuration options.
 *
 *
 */
function switchPanels(element) {
    var panel = $(element)
        .attr("class")
        .split(" ")[1]
        .substr(7);

    $(".pager-button").removeClass("active");
    $(".pager-button.button-" + panel).addClass("active");

    $(".instructions").html($(element).attr("title"));
    $(".option-panel").removeClass("active");
    $(".option-panel." + panel + "-panel").addClass("active");
}

/**
 *
 *
 * Hide color pickers for other color options.
 */
function colorPicker() {
    $(".color-specification-input")
        .not(".color-picker-standard")
        .prop("disabled", true)
        .css("display", "none");

    $(".color-option").click(function() {
        var colorOption = $(this)
            .attr("class")
            .substr(19);

        $(".color-picker-" + colorOption)
            .prop("disabled", false)
            .css("display", "block");

        $(".color-specification-input")
            .not(".color-picker-" + colorOption)
            .prop("disabled", true)
            .css("display", "none");
    });
}

/**
 * Initialize rendering with default values.
 *
 *
 */
function renderTrough() {
    var Trough = function(troughValues) {
        this.length = troughValues[0];
        this.width = troughValues[1];
        this.height = troughValues[2];
        this.wallThickness = troughValues[3];
        this.footHeight = troughValues[4];
        this.skirtFront = troughValues[5];
        this.skirtLeft = troughValues[6];
        this.skirtBack = troughValues[7];
        this.skirtRight = troughValues[8];
        this.colorOption = troughValues[9];
        this.colorSpecification = troughValues[10];
        this.doubleFrame = troughValues[11];
        this.profileFront = troughValues[12];
        this.profileLeft = troughValues[13];
        this.profileBack = troughValues[14];
        this.profileRight = troughValues[15];
        this.styropor = troughValues[16];
        this.vlies = troughValues[17];
        this.leca = troughValues[18];
        this.middlePiece = troughValues[19];
        this.rotationX = troughValues[20];
        this.rotationY = troughValues[21];
        this.zoomOffset = troughValues[22];
    };

    window.trough = new Trough([
        100,
        50,
        50,
        1.6,
        1.6,
        0,
        0,
        0,
        0,
        "Standardfarbe",
        "363d43",
        1.6,
        0,
        0,
        0,
        0,
        false,
        false,
        false,
        false,
        0.08,
        0.95,
        0
    ]);

    initializeRendering(trough);
}

/**
 * Calculation and re-rendering.
 *
 *
 */
function calculateTrough() {
    $(".modal, .modal-container.information").addClass("in");
    $(".modal-container.information h2").html("Der Trog wird berechnet");

    var form = $("#configuration-form").serializeArray();
    var fields = {};

    for (var i = 0; i < form.length; i++) {
        var name = form[i].name;
        var value = form[i].value;

        if (name) {
            fields[name] = value;
        }
    }

    fields["ral-color-name"] = $(".color-picker-ral option:selected").text();

    $.ajax({
        url: "/configurator/calculate",
        type: "post",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        data: fields,
        dataType: "json",
        success: function(data) {
            var volume = data["earth_volume"];
            var weight = data["weight_empty"];
            var weightEarth = data["weight_earth"];
            var bendingWall = data["bending_wall"];
            var bendingFloor = data["bending_floor"];
            var surface = data["surface"];
            trough.middlePiece = data["middle_piece"];
            var price = data["trough_price"];
            var error = data["error"];
            var wallCorrection = data["wall_thickness"];
            var doubleFrame = data["double_frame"];

            if (wallCorrection == 0.024) {
                $('input:radio[name="wall-thickness"][value=24]').prop(
                    "checked",
                    true
                );
                $('input:radio[name="wall-thickness"][value=16]').prop(
                    "checked",
                    false
                );
            } else {
                $('input:radio[name="wall-thickness"][value=16]').prop(
                    "checked",
                    true
                );
                $('input:radio[name="wall-thickness"][value=24]').prop(
                    "checked",
                    false
                );
            }

            //var thickness = wallCorrection * 100;
            var thickness = Number(
                $("input[name=wall-thickness]:checked").val() / 10
            );

            $(".price-box .price, #mobile-price-box .price").html(
                price.toFixed(2) + " €"
            );
            $(".price-box .price-vat .price-value, #mobile-price-box .price-vat .price-value").html((price * 1.2).toFixed(2) + " €");
            $(".error-wrapper h5").text(error);

            $(".paramValue.tn-weight").html(Math.round(weight));
            $(".paramValue.tn-weight-earth").html(Math.round(weightEarth));
            $(".paramValue.tn-weight-wet").html(
                Math.round((weightEarth / 1.2) * 1.8)
            );
            $(".paramValue.tn-volume").html(volume);
            $(".paramValue.tn-surface").html(surface);
            $(".paramValue.tn-bending-wall").html(
                Math.round(bendingWall * 1000)
            );
            $(".paramValue.tn-bending-floor").html(
                Math.round(bendingFloor * 1000)
            );

            trough.length = validateMeasures("length", 16, 200);
            trough.width = validateMeasures("width", 20, 120);
            trough.height = validateMeasures("height", 10, 100);
            trough.wallThickness = thickness;

            trough.footHeight = validateMeasures("foot-height", 1.6, 10);
            trough.skirtFront = validateMeasures("skirt-front", 0, 30);
            trough.skirtLeft = validateMeasures("skirt-left", 0, 30);
            trough.skirtBack = validateMeasures("skirt-back", 0, 30);
            trough.skirtRight = validateMeasures("skirt-right", 0, 30);

            trough.colorOption = $("input[name=color-option]:checked").val();

            switch (trough.colorOption) {
                case "Standardfarbe":
                    trough.colorSpecification = $(
                        "input[name=standard-ral]:checked"
                    ).val();
                    break;
                case "RAL-Farbe":
                    trough.colorSpecification = $(
                        ".color-specification-input.color-picker-ral option:selected"
                    ).val();
                    break;
                case "NCS-Farbe":
                    trough.colorSpecification = $(
                        ".color-specification-input.color-picker-ncs"
                    ).val();
                    break;
                case "Buntsteinputz":
                    trough.colorSpecification = $(
                        ".color-specification-input.color-picker-buntsteinputz"
                    ).val();
                    break;
                case "Reibputz":
                    trough.colorSpecification = $(
                        ".color-specification-input.color-picker-reibputz"
                    ).val();
                    break;
                default:
                    trough.colorSpecification = "363d43";
            }

            trough.doubleFrame =
                $("input[name=double-frame]:checked").val() / 10;
            trough.profileFront =
                $("input[name=profile-front]").prop("checked") == true
                    ? true
                    : false;
            trough.profileLeft =
                $("input[name=profile-left]").prop("checked") == true
                    ? true
                    : false;
            trough.profileBack =
                $("input[name=profile-back]").prop("checked") == true
                    ? true
                    : false;
            trough.profileRight =
                $("input[name=profile-right]").prop("checked") == true
                    ? true
                    : false;

            trough.styropor =
                $("input[name=styropor]").prop("checked") == true
                    ? true
                    : false;
            trough.vlies =
                $("input[name=vlies]").prop("checked") == true ? true : false;
            trough.leca =
                $("input[name=leca]").prop("checked") == true ? true : false;

            scene = null;
            $("canvas").remove();
            initializeRendering(trough);

            storeStats(fields, data, thickness, doubleFrame, wallCorrection);
        },
        error: function() {}
    });
}

/**
 * Save the values in the session storage for later retrieval
 * by the send offer function.
 *
 *
 */
function storeStats(fields, data, thickness, doubleFrame, wallCorrection) {
    if (typeof Storage !== "undefined") {
        sessionStorage.setItem("fields", JSON.stringify(fields));
        sessionStorage.setItem("volume", data["volume"]);
        sessionStorage.setItem("weightEmpty", data["weight_empty"]);
        sessionStorage.setItem("weightEarth", data["weight_earth"]);
        sessionStorage.setItem("bendingWall", data["bending_wall"]);
        sessionStorage.setItem("bendingFloor", data["bending_floor"]);
        sessionStorage.setItem("surface", data["surface"]);
        sessionStorage.setItem("middlePiece", data["middle_piece"]);
        sessionStorage.setItem("price", data["trough_price"]);
        sessionStorage.setItem("thickness", thickness);
        sessionStorage.setItem("doubleFrame", doubleFrame);
        sessionStorage.setItem("newWallThickness", wallCorrection);
    }

    return;
}

/**
 * Save the values in the session storage for later retrieval
 * by the send offer function.
 *
 *
 */
function fetchStats() {
    if (typeof Storage !== "undefined") {
        /**
         * If the user has not interacted with the form yet, a default calculation will be started.
         */
        if (sessionStorage.getItem("price") === null) {
            calculateTrough();
        }

        var stats = {
            fields: JSON.parse(sessionStorage.getItem("fields")),
            volume: sessionStorage.getItem("volume"),
            weightEmpty: sessionStorage.getItem("weightEmpty"),
            weightEarth: sessionStorage.getItem("weightEarth"),
            bendingWall: sessionStorage.getItem("bendingWall"),
            bendingFloor: sessionStorage.getItem("bendingFloor"),
            surface: sessionStorage.getItem("surface"),
            middlePiece: sessionStorage.getItem("middlePiece"),
            price: sessionStorage.getItem("price"),
            thickness: sessionStorage.getItem("thickness"),
            doubleFrame: sessionStorage.getItem("doubleFrame")
        };
    }

    return stats;
}

/**
 * Switching camera angles and zoom for scene.
 *
 *
 */
function cameraOptions() {
    $(".modal, .modal-container.information").addClass("in");
    $(".modal-container.information h2").html("Die Perspektive wird geändert");

    var cameraOption = $(this)
        .attr("class")
        .split(" ")[1];

    switch (cameraOption) {
        case "zoom-in":
            trough.zoomOffset -= 10;
            if (trough.zoomOffset < -10) trough.zoomOffset = -10;
            break;
        case "zoom-out":
            trough.zoomOffset += 10;
            if (trough.zoomOffset > 60) trough.zoomOffset = 60;
            break;
        case "camera-front":
            trough.rotationY += 0.5;
            break;
        case "camera-rear":
            trough.rotationY -= 0.5;
            break;
        case "camera-bottom":
            trough.rotationX += 0.3;
            break;
        case "camera-top":
            trough.rotationX -= 0.3;
            break;
    }

    scene = null;
    $("canvas").remove();
    initializeRendering(trough);
    console.clear();
}

/**
 * Check if the user has filled out all fields of the contact form.
 *
 *
 */
function requestReady() {
    $("#request-form input").each(function() {
        var element = $(this);

        if (element.val() == "") {
            $(".btn.request-offer").prop("disabled", true);

            return;
        }

        $(".btn.request-offer").prop("disabled", false);
    });
}

/**
 * Send the offer for the configured product via e-mail.
 *
 *
 */
function sendOffer() {
    var contact = $("#request-form").serializeArray();
    var stats = fetchStats();

    $.ajax({
        url: "/configurator/send-offer",
        type: "post",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        data: {
            contact: contact,
            stats: stats
        },
        dataType: "json",
        success: function(data) {
            setTimeout(function() {
                $("body").removeClass("modal-open");
                $(".modal")
                    .removeClass("show")
                    .css("display", "none");
                $(".modal-backdrop.show").remove();
            }, 3000);
        },
        error: function() {
            console.log("Error");
        }
    });
}
