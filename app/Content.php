<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Content extends Model
{
    /**
     * Return user-defined content from the DB
     *
     * @return array
     */
    public static function fetchContent() {
        $content = DB::table('content')->get();
       
        return $content;
    } 
}
